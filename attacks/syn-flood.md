# SYN Flood

## How it works
- TCP connection requires three-way-handshake (SYN -> SYN-ACK -> ACK)
- Attacker sends a high volume of SYN packets
- Server receives the SYN packet, expecting it to be a legitimate connection request. From there, it allocates resources for the connection (memory plus a port), and sends a SYN-ACK packet
- Server waits for the ACK packet, but never receives it. The server has a timeout set for how long it waits for a response before closing the connection, but until the connection will remain. This is referred to as a half-open connection, as it has not completed
- In the meantime, the attacker sends more and more SYN packets, exhausting all of the server's free connections
- By the time a legitimate user attempts to open a connection to the server, its resources are completely saturated with phony half-open connections, and it refuses all further incoming connection attempts

![SYN flood](https://www.imperva.com/learn/wp-content/uploads/sites/13/2019/01/syn-flood.jpg)

## Effectiveness
- SYN packets from bogus connection attempts are indistinguishable from legitimate SYN packets
- Exploits the connection limit set in place by the operating system
- Able to quickly exhaust system resources if the backlog queue has not been increased
- Even mitigating this kind of attack is expensive, there is no workaround to magically make the problem vanish

## Mitigation
### Rate limiting SYN packets per service
One possible mitigation attempt that rookies may take is rate limiting all incoming SYN packets to a specific service (i.e TCP port 80). The problem with this is that it cannot be done on a per-IP basis, since the attacker can either conduct this attack through a botnet or simply spoof the source IP address, hence the first "D" in DDoS (distributed). The likeliness of a single host sending enough SYN packets to be considered a SYN flood is not very high. Thus, one could attempt something like this using iptables:
```bash
iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW --dport 80 -m limit --limit 20/s --limit-burst 100 -j ACCEPT
# Deny all traffic that doesn't match this, or put a rule to catch all other new TCP traffic on port 80 and drop it...
```

However, if you rate limit all incoming SYN traffic regardless of the client, the token bucket will be filled up by illegitimate requests and leave future, innocent, requests to be discarded. While this may reduce system resource saturation, the attacker has completed their goal, which is to deny service.

### SYN cookies
According to [cr.yp.to](https://cr.yp.to/syncookies.html):

> A server that uses SYN cookies doesn't have to drop connections when
> its SYN queue fills up. Instead it sends back a SYN+ACK, exactly as if
> the SYN queue had been larger. (Exceptions: the server must reject TCP
> options such as large windows, and it must use one of the eight MSS
> values that it can encode.) When the server receives an ACK, it checks
> that the secret function works for a recent value of t, and then
> rebuilds the SYN queue entry from the encoded MSS.

Effectively, this will allow the server to accept incoming connection requests despite having a full SYN queue. On Linux, it can be enabled like this:
```bash
sysctl -w net.ipv4.tcp_syncookies=1
```

### Increasing the TCP SYN backlog
This determines how many half-open connections the server will keep before it starts refusing any more new connections. The Linux TCP SYN backlog can be increased with the following command. 16384 is simply used as a generic value. It is important to take into consideration that a server must allocate resources in order to open a connection. Setting this value too may lead to resource exhaustion or performance issues.
```bash
sysctl -w net.ipv4.tcp_max_syn_backlog=16384
```

### SYNPROXY
You may read up on what SYNPROXY is [here](https://gitlab.com/cryptio/ddos-research/-/blob/master/mitigation/netfilter/synproxy.md). To sum it up, SYNPROXY will make sure that the client actually completes a three-way-handshake before passing the request to the server, that is, it actually "does something" as opposed to simply sending a SYN packet and nothing more. While this luxury does have an overhead, you do not risk over-rate-limiting traffic regardless of whether or not it is legitimate.

### Evaluate who needs to open new connections
Maybe you are facing DDoS attacks that target your SSH server. Ask yourself: Who needs permission to use SSH on my system? Do other people require access, or can I simply limit the service based on a per-user basis? Reducing your attack surface by locking down services that shouldn't be Internet-facing is a simple yet effective way to mitigating DDoS attacks. Some may consider setting up a VPN server and only allowing incoming SSH session requests from the IP address of the VPN. 

## Research
[SYN Flood DDoS Attack](https://www.cloudflare.com/learning/ddos/syn-flood-ddos-attack/)

[SYN Flood DDoS Attacks](https://www.netscout.com/what-is-ddos/syn-flood-attacks)

[What is a TCP SYN Flood](https://www.imperva.com/learn/application-security/syn-flood/)

[SYN cookies](https://cr.yp.to/syncookies.html)