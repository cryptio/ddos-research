# iptables priority

## Process flow
![iptables process flow](https://s3.amazonaws.com/cp-s3/wp-content/uploads/2015/09/08085516/iptables-Flowchart.jpg)

## Dropping attacks as early as possible
- Less packet processing required: uses less CPU time
- Attacks that do not require stateful inspection should be dropped in the `raw PREROUTING` chain
- Packets that use conntrack must be handled no sooner than the `mangle PREROUTING` chain. This is where mitigation against TCP attacks requiring state information should be dropped, such as `NEW` packets that aren't TCP SYN:

    `iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP`
