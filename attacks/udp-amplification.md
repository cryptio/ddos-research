# UDP based amplification attacks
## How it works
### Amplification criteria
In order for an attacker to perform an amplification based attack, the two following conditions must be met:
- The protocol being used does not validate source IP addresses
- At least one of the commands for the service will generate a response that is larger than the request. Amplification specifically refers to the response size being amplified by a certain factor in comparison to the request size. If the response is essentially the same size, then it is simply reflection.

Unfortunately, a lot of UDP based applications such as NTP, DNS, Memcache, and more meet the above criteria.

![Amplification attack](https://blog.cloudflare.com/content/images/illustration-amplification-attack-ph3.png)

- First, the attacker will choose an application layer protocol such as NTP to target
- Next, they will scan the Internet for all publicly exposed NTP servers, and send a probe to each one
- If the service responds to the probe with the expected result, that being a response which is significantly larger than the request, then the IP address of this server gets added to an "amplification list" 
- After a large list of servers that may be used for amplification attacks has been built, one or more machines that support IP Header Modification (IPHM) may be used to execute the attack
- Each machine owned by the attacker will start iterating over the list of vulnerable devices, sending crafted UDP packets to each one with the source IP address set to that of the victim's
- All of the vulnerable services will respond to the received packets, which was actually sent by the attacker but is spoofed to show as belonging to the victim. All of these large responses being sent to the victim at once will generate a large throughput and saturate the target
## Effectiveness
Because the attacker only needs to send small requests to direct large responses towards the victim, this is an extremely effective method of launching a DDoS attack. There are many benefits to it:
- Unlike botnets, hundreds to thousands of compromised devices are not required: only a list of hosts running vulnerable UDP services and one or more spoofed servers is used
- The amplification factor, which varies between different services, means that the attacker does not need to use a lot of their own bandwidth in order to send a high amount of bandwidth towards the victim. As shown in the diagram, with NTP, ten 1Gbps devices capable of IP spoofing can generate an attack up to 500Mbps
- Harder to mitigate, as these are legitimate servers on the Internet that are simply misconfigured

## Mitigation
### Blocking vulnerable UDP based protocols based on their source port
Lists of application-layer protocols that are most commonly used for DRDoS (Distributed Reflection Denial of Service) attacks can be found in places such as the following:

[UDP-Based Amplification Attacks](https://www.us-cert.gov/ncas/alerts/TA14-017A)

[How to Defend Against Amplified Reflection DDoS Attacks](https://www.a10networks.com/blog/how-defend-against-amplified-reflection-ddos-attacks/)

[AMP-Research](https://github.com/Phenomite/AMP-Research)

If you are sure that these services do not need to be contacting your network, then you may safely block the source ports that such protocols use.

### Filtering out traffic based on the application
A more precise way of blocking UDP-based amplification attack traffic is to identify patterns in the traffic. Most protocols, such as DNS, have a packet structure that they conform to. Using this information, you may use packet inspection to drop traffic from these services based on certain values present in the payload.
Let's look at DNS as an example. The following article explains how the DNS `ANY` query type was originally implemented to aid in testing and debugging, and how no legitimate application should ever use it: 

[Deprecating the DNS ANY meta-query type](https://blog.cloudflare.com/deprecating-dns-any-meta-query-type/)

DNS ANY is one of the biggest responses that DNS servers give out, making it perfect for use in amplification attacks. [CISA](https://www.us-cert.gov/ncas/alerts/TA14-017A) claims that the bandwidth amplification factor (BAC) may be anywhere between 24x to 54x the original request size! 

Fortunately, we now know that the DNS ANY query type is safe to block and is a common threat. Thus, we can drop all DNS answers that are responding to ANY:

```bash
iptables -t raw -A PREROUTING -p udp --sport 53 -m string --from 40 --algo bm --hex-string '|00 00 ff 00 01|' -j DROP
```

If you are not familiar with iptables and its modules such as `sport` and `string`, it is recommended that you take the time to learn about these items. Essentially, what the above rule does is this:
- Looks for all incoming UDP packets from port 53 (typical DNS traffic)
- Inspect the packet's content, with the offset beginning 40 bytes from the start of the IP header
- Match if the hexadecimal value `00 00 ff 00 01`is present between the offset and the end of the packet
- Drop the packet if it matches all of the above attributes

How the offset was calculated can be found [here](https://forums.centos.org/viewtopic.php?t=62148):
- IP header length is 20 bytes minimum
- UDP header has fixed length of 8 bytes
- DNS header with as few options as possible is 12 bytes:
 
	| Field | Length |
	|--|--|
	| Transaction ID | 2 bytes |
	| Flags | 2 bytes |
	| Questions | 2 bytes |
	| Answer RRs | 2 bytes |
	| Authority RRs | 2 bytes |
	| Additional RRs | 2 bytes |
- DNS headers are variable length, but we know that the first 12 byte will all be the same data and present every time. This means that we don't have to look any sooner than our offset of 40 bytes for the ANY query type.

### Promote the security and hardening of such devices
The most sensible solution is for the owners of these devices to take the necessary precautions in making sure that the software running on their systems are up-to-date, properly configured, and secured.
