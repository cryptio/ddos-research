# DDoS Research

My personal research on DDoS attacks and mitigation techniques

## Disclaimers
- This repository is entirely for educational purposes. It attempts to educate others on common DDoS attack vectors, as well as the possible techniques and tools to defend against them. I do not condone using any of the information contained within this to conduct attacks or perform unauthorized stress testing. In the event that such an action arises, I take no liability for any damages or consequences incurred.
- Only the research that I choose to publicize will be included here. If this does not suffice, I recommend purchasing my [Attackalyzer](https://attackalyzer.cf/docs) service. It makes use of all DDoS research and analysis skills I possess, and every DDoS attack signature that I am aware of is registered in it.


## Table of Contents
- Attacks
	- Generic:
		- [TCP Reflection](attacks/tcp-reflection.md)
		- [UDP Amplification](attacks/udp-amplification.md)
		- [SYN Flood](attacks/syn-flood.md)
	- Specialized:
		- [OVH-RAPE](attacks/ovh-rape)
		- [KILL-ALL](attacks/kill-all.md)
- Mitigation
    - [netfilter](mitigation/netfilter)
    - [xdp](mitigation/xdp)